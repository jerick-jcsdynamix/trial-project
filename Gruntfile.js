module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: "\n"
            },
            publicDist: {
                src: [
                    'vendor/bower/jquery/dist/jquery.js',
                    'vendor/bower/bootstrap/dist/js/bootstrap.min.js',
                    'vendor/yiisoft/yii2/assets/yii.js',
                    'vendor/yiisoft/yii2/assets/yii.validation.js',
                    'vendor/yiisoft/yii2/assets/yii.activeForm.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/bootbox/bootbox.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/lazy/jquery.lazy.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/base64/jquery.base64.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/moment/moment-with-locales.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/datetimepicker/bootstrap-datetimepicker.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/iCheck/icheck.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/select2/select2.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/jcs.min.js',
                    'public_html/js/frontend/site.js',
                    'public_html/js/frontend/*.js',
                    '!public_html/js/frontend/*.min.js'
                ],
                dest: 'public_html/js/dist/public/<%= pkg.name %>.js'
            },
            adminDist: {
                src: [
                    'vendor/bower/jquery/dist/jquery.js',
                    'vendor/bower/bootstrap/dist/js/bootstrap.min.js',
                    'vendor/yiisoft/yii2/assets/yii.js',
                    'vendor/yiisoft/yii2/assets/yii.validation.js',
                    'vendor/yiisoft/yii2/assets/yii.activeForm.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/bootbox/bootbox.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/lazy/jquery.lazy.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/base64/jquery.base64.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/moment/moment-with-locales.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/datetimepicker/bootstrap-datetimepicker.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/iCheck/icheck.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/vendor/plugins/select2/select2.min.js',
                    'vendor/jcsdynamix/yii2-jcsjs/js/jcs.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/metisMenu/jquery.metisMenu.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/slimscroll/jquery.slimscroll.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/ckeditor/ckeditor.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/ckeditor/adapters/jquery-adapter.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/inspinia.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/pace/pace.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.tooltip.min.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.spline.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.resize.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.pie.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.symbol.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.time.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/flot/jquery.flot.categories.js',
                    'vendor/jcsdynamix/yii2-inspinia/js/vendor/plugins/chartJs/Chart.min.js',
                    'public_html/js/admin/admin.js',
                    'public_html/js/admin/*.js',
                    '!public_html/js/admin/*.min.js'
                ],
                dest: 'public_html/js/dist/admin/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            publicDist: {
                files: [
                    {
                        src: ['<%= concat.publicDist.dest %>'],
                        dest: 'public_html/js/dist/public/<%= pkg.name %>.min.js'
                    }
                ]
            },
            adminDist: {
                files: [
                    {
                        src: ['<%= concat.adminDist.dest %>'],
                        dest: 'public_html/js/dist/admin/<%= pkg.name %>.min.js'
                    }
                ]
            }
        },
        jshint: {
            files: [
                'Gruntfile.js',
                'public_html/js/**/*.js',
                '!public_html/js/**/*.min.js',
                '!public_html/js/dist/**/*.js',
                '!public_html/js/vendor/**/*.min.js'
            ],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        sass: {
            options: {
                style: 'compressed'
            },
            publicDist: {
                files: [{
                    expand: true,
                    cwd: 'public_html/css/frontend',
                    src: ['*.sass'],
                    dest: 'public_html/css/frontend/',
                    ext: '.min.css'
                }]
            },
            adminDist: {
                files: [{
                    expand: true,
                    cwd: 'public_html/css/admin',
                    src: ['*.sass'],
                    dest: 'public_html/css/admin/',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            publicScripts: {
                files: ['<%= concat.publicDist.src %>'],
                tasks: ['jshint', 'concat:publicDist', 'uglify:publicDist']
            },
            // adminScripts: {
            //     files: ['<%= concat.adminDist.src %>'],
            //     tasks: ['jshint', 'concat:adminDist', 'uglify:adminDist']
            // },
            publicCss: {
                files: ['public_html/css/frontend/**/*.sass'],
                tasks: ['sass:publicDist']
            },
            adminCss: {
                files: ['public_html/css/admin/**/*.sass'],
                tasks: ['sass:adminDist']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');

    grunt.registerTask('default', ['watch']);
};