<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../components/JCSApplication.php');

Yii::$classMap['yii\helpers\Html']         = '@jcsdynamix/helpers/Html.php';
Yii::$classMap['yii\helpers\Inflector']    = '@jcsdynamix/helpers/Inflector.php';
Yii::$classMap['yii\helpers\StringHelper'] = '@jcsdynamix/helpers/StringHelper.php';
Yii::$classMap['yii\helpers\Url']          = '@jcsdynamix/helpers/Url.php';

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\JCSApplication($config))->run();
