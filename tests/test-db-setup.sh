php codeception/bin/yii migrate --migrationPath=@jcsdynamix/authorisation/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/versioning/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/image/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/timezones/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/language/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/multisites/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/pages/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/blogs/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/testimonials/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/casestudies/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/categories/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/guidearticles/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/enquiries/migrations --interactive=0
php codeception/bin/yii migrate --migrationPath=@jcsdynamix/loggable/migrations --interactive=0
php codeception/bin/yii migrate --interactive=0