<?php

/* @var $scenario Codeception\Scenario */

$I = new FunctionalTester($scenario);
$I->wantTo('ensure that home page works');

$I->amOnPage(Yii::$app->homeUrl);

// Title
$I->seeInTitle('Home');

// Nav bar links
// $I->seeLink('Home');

// Check Google Analytics
$I->see("ga('create'", 'script');
$I->see("UA-", 'script');
