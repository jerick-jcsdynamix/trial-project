<?php

namespace tests\codeception\unit\models;

use jcsdynamix\login\models\LoginForm;

use Yii;
use yii\codeception\TestCase;
use Codeception\Specify;

class LoginFormTest extends TestCase
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function tearDown()
    {
        Yii::$app->user->logout();
        parent::tearDown();
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Test attempt to login with credentials that match no existing user.
     *
     * @return void
     */
    public function testLoginNoUser()
    {
        $model = new LoginForm([
            'email'    => 'non_existent_email',
            'password' => 'non_existent_password',
        ]);

        $this->specify(
            'user should not be able to login, without identity',
            function () use ($model) {
                expect('model login fails', $model->login())->false();
                expect('user not logged in', Yii::$app->user->isGuest)->true();
            }
        );
    }

    /**
     * Test that user cannot login with correct email but incorrect password.
     *
     * @return void
     */
    public function testLoginWrongPassword()
    {
        $model = new LoginForm([
            'email'    => 'jcsdev.test@gmail.com',
            'password' => 'wrong_password',
        ]);

        $this->specify(
            'user should not be able to login with wrong password',
            function () use ($model) {
                expect('model login fails', $model->login())->false();
                expect('error message set', $model->errors)->hasKey('password');
                expect('user not logged in', Yii::$app->user->isGuest)->true();
            }
        );
    }

    /**
     * Tests that user can login with valid credentials.
     *
     * @return void
     */
    public function testLoginValid()
    {
        $model = new LoginForm([
            'email'    => 'jcsdev.test@gmail.com',
            'password' => 'dynamix',
        ]);

        $this->specify(
            'user should be able to login with valid credentials',
            function () use ($model) {
                expect('model logs user in', $model->login())->true();
                expect('no errors', $model->errors)->hasntKey('password');
                expect('user is logged in', Yii::$app->user->isGuest)->false();
            }
        );
    }
}
