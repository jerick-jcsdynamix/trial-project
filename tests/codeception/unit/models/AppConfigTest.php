<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use yii\helpers\ArrayHelper;
use Codeception\Specify;

class AppConfigTest extends TestCase
{
    use Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * Tests that application params do not have test settings enabled.
     *
     * @return void
     */
    public function testTestSettingsDisabled()
    {
        $this->specify('test emails disabled', function () {
            $emails = ArrayHelper::getValue(Yii::$app->params, 'test_email');
            expect('test_emails is not on', !$emails)->true();
        });
    }
}
