<?php

$adminFolder = 'admin';

if (array_key_exists('admin_folder', Yii::$app->params)) {
    $adminFolder = Yii::$app->params['admin_folder'];
}

?>

<li class="<?= Yii::$app->activeMenu('home') ?>">
	<a href="/<?= $adminFolder ?>/">
		<i class="fa fa-th-large fa-fw"></i>
		<span class="nav-label">Dashboard</span>
	</a>
</li>

<?php if (Yii::$app->allowed('category:category/index')) : ?>
    <li class="<?= Yii::$app->activeMenu('category') ?>">
        <a href="/<?= $adminFolder ?>/categories/">
            <i class="fa fa-tags fa-fw"></i>
            <span class="nav-label"> Categories </span>
        </a>
    </li>
<?php endif ?>

<?php if (Yii::$app->allowed('enquiries:enquiry/index')) : ?>
    <li class="<?= Yii::$app->activeMenu('enquiry') ?>">
        <a href="/<?= $adminFolder ?>/enquiries/">
            <i class="fa fa-envelope-o fa-fw"></i>
            <span class="nav-label"> Enquiries </span>
        </a>
    </li>
<?php endif ?>

<?php if (Yii::$app->allowedIn([
    'testimonials:testimonial/index',
    'case-study:case-study/index',
    'blog:blog/index',
    'guide-article:guide-article/index',
    'page:page/index',
    'page:block/index',
    'page:template/index',
])) : ?>
    <li class="<?= Yii::$app->activeMenuIn([
            'testimonial',
            'case-study',
            'blog',
            'page',
            'block',
            'template',
            'guide-article',
        ]) ?>">
        <a href="index.html">
            <i class="fa fa-files-o fa-fw"></i>
            <span class="nav-label">Content</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
            <?php if (Yii::$app->allowed('testimonials:testimonial/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('testimonial') ?>">
                    <a href="/<?= $adminFolder ?>/testimonials/">Testimonials</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('case-study:case-study/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('case-study') ?>">
                    <a href="/<?= $adminFolder ?>/case-studies/">Case Studies</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('blog:blog/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('blog') ?>">
                    <a href="/<?= $adminFolder ?>/blogs/">Blog Articles</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('guide-article:guide-article/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('guide-article') ?>">
                    <a href="/<?= $adminFolder ?>/guide-articles/">
                        Guide Articles
                    </a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('page:page/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('page') ?>">
                    <a href="/<?= $adminFolder ?>/pages/">Pages</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('page:block/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('block') ?>">
                    <a href="/<?= $adminFolder ?>/blocks/">Blocks</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('page:template/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('template') ?>">
                    <a href="/<?= $adminFolder ?>/templates/">Templates</a>
                </li>
            <?php endif ?>
        </ul>
    </li>
<?php endif; ?>

<?php if (Yii::$app->allowedIn([
    'language:language/index',
    'timezone:timezone/index',
])) : ?>
    <li class="<?= Yii::$app->activeMenuIn(['language', 'timezone']) ?>">
        <a href="index.html">
            <i class="fa fa-map-marker fa-fw"></i>
            <span class="nav-label">Locale Management</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
            <?php if (Yii::$app->allowed('language:language/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('language') ?>">
                    <a href="/<?= $adminFolder ?>/languages/">Languages</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('timezone:timezone/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('timezone') ?>">
                    <a href="/<?= $adminFolder ?>/timezones/">Timezones</a>
                </li>
            <?php endif ?>
        </ul>
    </li>
<?php endif; ?>

<?php if (Yii::$app->allowedIn([
    'website:website/index',
    'aliased-domain:aliased-domain/index',
    'website-redirect:website-redirect/index',
])) : ?>
    <li class="<?= Yii::$app->activeMenuIn(['website', 'website-redirect', 'aliased-domain']) ?>">
        <a href="index.html">
            <i class="fa fa-globe fa-fw"></i>
            <span class="nav-label">Web Management</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
            <?php if (Yii::$app->allowed('website:website/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('website') ?>">
                    <a href="/<?= $adminFolder ?>/websites/">Websites</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('aliased-domain:aliased-domain/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('aliased-domain') ?>">
                    <a href="/<?= $adminFolder ?>/aliased-domains/">Aliased Domains</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('website-redirect:website-redirect/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('website-redirect') ?>">
                    <a href="/<?= $adminFolder ?>/website-redirects/">Website Redirects</a>
                </li>
            <?php endif ?>
        </ul>
    </li>
<?php endif; ?>

<?php if (Yii::$app->allowedIn([
    'user:user/index',
    'user:role/index',
])) : ?>
    <li class="<?= Yii::$app->activeMenuIn(['user', 'role']) ?>">
        <a href="index.html">
            <i class="fa fa-users fa-fw"></i>
            <span class="nav-label">User Management</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="nav nav-second-level">
            <?php if (Yii::$app->allowed('user:user/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('user') ?>">
                    <a href="/<?= $adminFolder ?>/users/">Users</a>
                </li>
            <?php endif ?>
            <?php if (Yii::$app->allowed('user:role/index')) : ?>
                <li class="<?= Yii::$app->activeMenu('role') ?>">
                    <a href="/<?= $adminFolder ?>/roles/">Roles</a>
                </li>
            <?php endif ?>
        </ul>
    </li>
<?php endif; ?>
