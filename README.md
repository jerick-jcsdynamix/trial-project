JCS Yii2 General CMS Project Template
======================================

Provide a base template to begin development of a new CMS project with. The
aim of this project template is to pull together all of JCS' custom built Yii
extensions and handle all the repeatable config so there is as little to do as
possible when starting a new project.

This particular template is for a general CMS website. It includes all the
CMS related extensions created by JCS Dynamix. If you need a more scaled back
template with just a really basic subset of the install extensions it would be
best to start with a different project template. In that case, if one doesn't
exist you can use this template as a reference to create one with fewer
extensions installed.

FEATURES INCLUDED VIA EXTENSIONS
---------------------------------

1. Authorisation and Logins
2. Users
3. Languages (not multilingual objects though)
4. Websites, Aliased Domains and Website Redirects
5. Blogs
6. Testimonials
7. Case Studies
8. Categories
9. Guide Articles
10. Enquiries
11. Timezones
12. Dashboard
13. Galleries
14. Pages, Blocks and Page Templates


REQUIREMENTS
------------

* php >=5.4.0
* yiisoft/yii2 >=2.0.5
* yiisoft/yii2-bootstrap
* yiisoft/yii2-swiftmailer
* jcsdynamix/yii2-language dev-master
* jcsdynamix/yii2-users dev-master
* jcsdynamix/yii2-gallery-manager dev-master
* jcsdynamix/yii2-pages dev-master
* jcsdynamix/yii2-multisites dev-master
* jcsdynamix/yii2-active-menu-trait dev-master
* jcsdynamix/yii2-blogs dev-master
* jcsdynamix/yii2-testimonials dev-master
* jcsdynamix/yii2-categories dev-master
* jcsdynamix/yii2-guide-articles dev-master
* jcsdynamix/yii2-dashboard dev-master
* jcsdynamix/yii2-enquiries dev-master
* jcsdynamix/yii2-timezones dev-master
* jcsdynamix/yii2-case-studies dev-master


INSTALLATION
------------

### Manual Install Via Git




CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
