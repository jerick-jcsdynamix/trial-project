<?php

namespace app\models;

use \Closure;
use \ReflectionFunction;

use Yii;
use yii\helpers\StringHelper;

/**
* Authorisation class to be used in the authorisation of user actions within the
* LTN system.
*/
class Authorisation
{

    /**
     * Stores the ID of the user passed to the constructor
     *
     * @var integer
     */
    private $userID;

    /**
     * Used for identifying SuperAdmin users that can access anything in the
     * system.
     *
     * @var boolean
     */
    private $allowAll = false;

    /**
     * Used for identifying Admin users that can access anything except certain
     * sections
     *
     * @var boolean
     */
    private $allowAllExcept = false;

    /**
     * Stores the list of blacklisted permissions. Access to anything in this
     * list will be denied unless the user is a SuperAdmin user i.e.
     * $this->allowAll is true.
     *
     * @var array
     */
    private $blacklistedActions = [];

    /**
     * Stores the list of allowed actions. If the user isn't blacklisted from
     * the action and it exists in this list they will be granted permission.
     * Otherwise they will be denied.
     *
     * @var array
     */
    private $allowedActions = [];

    /**
     * The constructor for this class must be provided a User object. This is
     * generally the currently logged in user. Using this User, it builds the
     * lists for blacklisted and allowed actions.
     *
     * It is from these lists and the allowAll and allowAllExcept flags that the
     * system can base its decisions on whether to authorize an action.
     *
     * The permissions are added via calls to the allow function. See its
     * implementation for exactly how this occurs and the structure of the
     * resulting list.
     *
     * Blacklisted actions can be set via the disallow which accepts the same
     * parameters as the allow function.
     *
     * Authorization checks are performed via the allowed function.
     *
     * @param User $user  Generally the currently logged in user
     */
    public function __construct($user)
    {
        // ASSIGN GUEST PERMISSIONS
        // $this->allow('modules', 'controllers', 'actions', 'function');
        $this->allow('admin', 'login', 'login');
        // $this->allow('admin', 'languages', 'index');

        // ASSIGN AUTHENTICATED PERMISSIONS
        if ($user) {
            $this->userID = $user->id;

            // Add calls to $this->allow() and $this->disallow() to set
            // access permissions for general users.

            // ASSIGN SUPER ADMIN ONLY PERMISSIONS
            if ($user->super_admin) {
                $this->allowAll = true;
                return;
            }

            // ASSIGN ADMIN ONLY PERMISSIONS
            if ($user->hasRole('Admin')) {
                $this->allowAllExcept = true;
            }

            // Find and assign all permissions or blacklisted permissions for users
            // roles.
            foreach ($user->roles as $role) {
                $this->processRole($role);
            }
        }
    }

    /**
     * Processes a Role's permissions allowing or denying access accordingly.
     *
     * @param  Role     $role
     *
     * @return void
     */
    protected function processRole($role)
    {
        foreach ($role->permissions as $permission) {
            $modules     = null;
            $controllers = null;
            $actions     = null;
            $code        = null;
            $callback    = null;

            if ($permission->modules) {
                $modules = explode(',', $permission->modules);
            }

            if ($permission->controllers) {
                $controllers = explode(',', $permission->controllers);
            }

            if ($permission->actions) {
                $actions = explode(',', $permission->actions);
            }

            $code = "return function(\$access, \$model, \$property) {
                {$permission->callback}
            };";

            if ($code) {
                $callback = eval($code);
            }

            if ($permission->blacklisted) {
                $this->disallow($modules, $controllers, $actions, $callback);
            } else {
                $this->allow($modules, $controllers, $actions, $callback);
            }
        }
    }

    /**
     * Adds permissions to the allowed actions array.
     *
     * @param  string|array $modules
     * @param  string|array $controllers
     * @param  string|array $actions
     * @param  Closure      $callback    Callback function must have signature:
     *                                   function($model, $property = null)
     *                                   and return a boolean
     *
     * @return void
     */
    protected function allow(
        $modules,
        $controllers = null,
        $actions = null,
        $callback = null
    ) {
        $modules     = is_array($modules) ? $modules : (array)$modules;
        $actions     = is_array($actions) ? $actions : (array)$actions;
        $controllers = is_array($controllers) ? $controllers
                                              : (array)$controllers;

        // Start permissions assignment with Modules. Even no module (i.e. the
        // default app is ok as '' since we will still end up with unique key
        // e.g. ':controller/action').
        foreach ($modules as $module) {
            if (empty($controllers)) {
                // No controllers so full access granted to module.
                $this->allowedActions[$module] = true;
            } else {
                foreach ($controllers as $controller) {
                    $key = $module . ':' . $controller;
                    if (empty($actions)) {
                        // No actions so full access granted for this controller
                        // in the current module.
                        $this->allowedActions[$key] = true;
                    } else {
                        foreach ($actions as $action) {
                            // Grant access for this module>controller>action if
                            // no callback otherwise assign callback.
                            $key = $module . ':' . $controller . '/' . $action;

                            if ($callback) {
                                $this->allowedActions[$key] = $callback;
                            } else {
                                $this->allowedActions[$key] = true;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Blacklists actions for the supplied modules and controllers.
     *
     * @param  array|string $modules
     * @param  array|string $controllers
     * @param  array|string $actions
     * @param  Closure      $callback    Callback function must have signature:
     *                                   function($model, $property = null)
     *                                   and return a boolean
     *
     * @return void
     */
    protected function disallow(
        $modules,
        $controllers = null,
        $actions = null,
        $callback = null
    ) {
        $modules     = is_array($modules) ? $modules : (array)$modules;
        $actions     = is_array($actions) ? $actions : (array)$actions;
        $controllers = is_array($controllers) ? $controllers
                                              : (array)$controllers;

        foreach ($modules as $module) {
            if (empty($controllers)) {
                // No controllers so all access to module denied.
                $this->blacklistedActions[$module] = true;
            } else {
                foreach ($controllers as $controller) {
                    $key = $module . ':' . $controller;

                    if (empty($actions)) {
                        // No actions so all access for this controller is
                        // denied.
                        $this->blacklistedActions[$key] = true;
                    } else {
                        foreach ($actions as $action) {
                            // Deny access for this module>controller>action if
                            // no callback otherwise assign callback.
                            $key = $module . ':' . $controller . '/' . $action;

                            if ($callback) {
                                $this->blacklistedActions[$key] = $callback;
                            } else {
                                $this->blacklistedActions[$key] = true;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Determines if the requested action is permitted given the permissions
     * this object represents.
     *
     * @param  string $access
     * @param  mixed $model
     * @param  string $property
     *
     * @return boolean
     */
    public function allowed($access, $model = null, $property = null)
    {
        // Super User
        if ($this->allowAll) {
            return true;
        }

        // Admin ( Allow all except )
        if ($this->allowAllExcept) {
            return $this->notInBlacklist($access, $model, $property);
        }

        // General
        return ($this->notInBlacklist($access, $model, $property) &&
                $this->inAllowedActions($access, $model, $property));
    }

    /**
     * Determines whether a requested action is within the allowed actions.
     *
     * @param  string $access
     * @param  mixed $model
     * @param  string $property
     *
     * @return boolean
     */
    protected function inAllowedActions(
        $access,
        $model = null,
        $property = null
    ) {
        $debug = $access;
        if ($model) {
            $debug = $access . ' | ' . $model->formName() . ': ' . $model->id;
        }

        // No permissions set...deny all.
        if (empty($this->allowedActions)) {
            return false;
        }

        // Split the access string into its required parts.
        $module = $controller = $action = '';
        $this->explodeAccess($access, $module, $controller, $action);

        // Check module wide permission.
        if (isset($this->allowedActions[$module]) &&
            $this->allowedActions[$module] === true
        ) {
            return true;
        }

        // Check for controller wide permission.
        $key = $module . ':' . $controller;

        if (isset($this->allowedActions[$key]) &&
            $this->allowedActions[$key] === true
        ) {
            return true;
        }

        // Check for action specific permission.
        $key = $module . ':' . $controller . '/' . $action;

        if (isset($this->allowedActions[$key])) {
            $permission = $this->allowedActions[$key];

            // If specific action permission is true we have our result.
            if ($permission === true) {
                return true;
            }

            // Otherwise if the action permission is a callable anonymous
            // function. Execute it returning the results.
            if (is_a($permission, 'Closure')) {
                return $permission($access, $model, $property);
            }
        }

        return false;
    }

    /**
     * Takes an access string and splits this into module, controller and action
     * names according to the pattern: 'module:controller/action'.
     *
     * @param  string $access
     * @param  string &$module
     * @param  string &$controller
     * @param  string &$action
     *
     * @return void
     */
    protected function explodeAccess($access, &$module, &$controller, &$action)
    {
        $module = '';
        $parts  = explode(':', $access);

        if (count($parts) == 1) {
            $access = $parts[0];
        } else {
            $module = $parts[0];
            $access = $parts[1];
        }

        $parts = explode('/', $access);
        if (count($parts) == 1) {
            $controller = $parts[0];
        } else {
            $controller = $parts[0];
            $action = $parts[1];
        }
    }

    /**
     * Checks to see if a particular access request hasn't been blacklisted.
     *
     * @return boolean
     */
    protected function notInBlacklist($access, $model = null, $property = null)
    {
        // No items in blacklist.
        if (empty($this->blacklistedActions)) {
            return true;
        }

        // Split the access string into its required parts.
        $module = $controller = $action = '';
        $this->explodeAccess($access, $module, $controller, $action);

        // Check module wide blacklist
        if (isset($this->blacklistedActions[$module]) &&
            $this->blacklistedActions[$module] === true) {
            return false;
        }

        // Check for controller wide blacklist.
        $key = $module . ':' . $controller;
        if (isset($this->blacklistedActions[$key]) &&
            $this->blacklistedActions[$key] === true) {
            return false;
        }

        // Check for action specific blacklist.
        $key = $module . ':' . $controller . '/' . $action;

        if (isset($this->blacklistedActions[$key])) {
            // If specific action blacklist is true we have our result.
            if ($this->blacklistedActions[$key] === true) {
                return false;
            }

            // Otherwise if the action permission is a callable anonymous
            // function. Execute it returning the results.
            if (get_class($this->blacklistedActions[$key]) == 'Closure') {
                if ($this->blacklistedActions[$key]($access, $model, $property)) {
                    return false;
                }
            }
        }

        return true;
    }
}
