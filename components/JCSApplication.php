<?php

namespace yii\web;

use \DateTime;

use app\models\Authorisation;
use jcsdynamix\activemenutrait\ActiveMenuTrait;
use jcsdynamix\authorisation\AppAuthorisationTrait;
use jcsdynamix\language\models\Lanuage;

use Yii;

/**
 * This class overrides the default Yii Web Application class so as to provide a
 * convenient approach to being able to call user authorisation functions and
 * provide app wide caching of the current user's permissions.
 */
class JCSApplication extends \yii\web\Application
{
    use AppAuthorisationTrait;
    use ActiveMenuTrait;

    public $class;
}
