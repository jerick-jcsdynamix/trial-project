<?php

return [
    'add_module_routes' => [
        'aliased-domain',
        'block',
        'blogs',
        'case-studies',
        'dashboard',
        'guide-articles',
        'categories',
        'enquiries',
        'images',
        'languages',
        'login',
        'page',
        'public-blogs',
        'public-case-studies',
        'roles',
        'template',
        'testimonials',
        'timezones',
        'users',
        'website-redirect',
        'website',
    ],
    'admin_email' => 'jcsdev.test@gmail.com',
    'admin_folder' => 'admin',
];
