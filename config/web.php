<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath'   => dirname(__DIR__),
    'bootstrap'  => ['log'],
    'authClass'  => 'app\models\Authorisation',
    'timeZone'   => 'Australia/Brisbane',
    'aliases'   => [
        'webroot/assets' => dirname(__DIR__) . '/public_html/assets',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'MqsrB1VoYRMZWYzpZAlUkVGBynFiM-hz',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'jcsdynamix\authorisation\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/admin/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'showScriptName'      => false,
            'suffix' => '/',
            'rules'  => [
                '' => 'site/index',
                [
                    'pattern' => 'index-test',
                    'route'   => 'site/index',
                    'suffix'  => '.php'
                ],
                'site' => 'site/index',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<controller>/<action>' => '<controller>/<action>',
            ]
         ],
    ],
    'params' => $params,
    'modules' => [
        'language' => [
            'class' => 'jcsdynamix\language\Module',
        ],
        'image' => [
            'class' => 'jcsdynamix\imagemanager\Module',
        ],
        'multisites' => [
            'class' => 'jcsdynamix\multisites\Module',
        ],
        'login' => [
            'class' => 'jcsdynamix\login\Module',
        ],
        'pages' => [
            'class' => 'jcsdynamix\pages\Module',
        ],
        'blogs' => [
            'class' => 'jcsdynamix\blogs\Module',
        ],
        'users' => [
            'class' => 'jcsdynamix\users\Module',
        ],
        'testimonials' => [
            'class' => 'jcsdynamix\testimonials\Module',
        ],
        'case-studies' => [
            'class' => 'jcsdynamix\casestudies\Module',
        ],
        'guide-articles' => [
            'class' => 'jcsdynamix\guidearticles\Module',
        ],
        'categories' => [
            'class' => 'jcsdynamix\categories\Module',
        ],
        'dashboard' => [
            'class' => 'jcsdynamix\dashboard\Module',
        ],
        'enquiries' => [
            'class' => 'jcsdynamix\enquiries\Module',
        ],
        'timezones' => [
            'class' => 'jcsdynamix\timezones\Module',
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
